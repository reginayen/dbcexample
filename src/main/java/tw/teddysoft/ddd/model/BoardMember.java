package tw.teddysoft.ddd.model;

import tw.teddysoft.ddd.core.ValueObject;

public class BoardMember extends ValueObject {
	private BoardId boardId;
	private String userId;
	private BoardRole boardRole;

	BoardMember(BoardRole boardRole, BoardId boardId, String userId) {
		this.boardRole = boardRole;
		this.boardId = boardId;
		this.userId = userId;
	}

	public BoardId getBoardId() {
		return boardId;
	}
	public String getUserId() {
		return userId;
	}

	public BoardRole getBoardRole() {
		return boardRole;
	}

}
