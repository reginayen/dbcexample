package tw.teddysoft.ddd.model;

import tw.teddysoft.ddd.core.AggregateRoot;
import tw.teddysoft.ddd.core.DateProvider;
import tw.teddysoft.ddd.core.DomainEvent;

import java.util.*;
import java.util.function.Consumer;

import static tw.teddysoft.dbc.Contract.*;
import static java.lang.String.format;

public class Board extends AggregateRoot<BoardId> {
	private String name;
	private String teamId;
	private List<BoardMember> boardMembers;
	private List<CommittedWorkflow> committedWorkflows;

	private final Map<Class<? extends BoardEvents>, Consumer<BoardEvents>> eventHandlers = new HashMap<>();

	public Board(List<? extends DomainEvent> domainEvents){
		super();
		requireNotNull("Domain events", domainEvents);

		configEventHandlers();
		domainEvents.forEach( x-> apply(x));
		clearDomainEvents();
	}

	public Board(String teamId, BoardId boardId, String name) {
		super(boardId);
		requireNotNull("Team id", teamId);
		requireNotNull("Board name", name);

		configEventHandlers();
		apply(new BoardEvents.BoardCreated(teamId, boardId, name, UUID.randomUUID(), DateProvider.now()));
	}

	@Override
	public void markAsDeleted(String userId) {
		requireNotNull("User id", userId);

		apply(new BoardEvents.BoardDeleted(teamId, getBoardId(), userId, UUID.randomUUID(), DateProvider.now()));

		ensure("Board is marked as deleted", isDeleted);
	}

	public void rename(String newName) {
		requireNotNull("Board name", newName);

		if(getName().equals(newName)){
			return;
		}
		apply(new BoardEvents.BoardRenamed(
				teamId,
				getBoardId(),
				newName,
				UUID.randomUUID(),
				DateProvider.now()));

		ensure(format("Board name is '%s'", newName), () -> getName().equals(newName));
	}

	public void commitWorkflow(WorkflowId workflowId) {
		requireNotNull("Workflow id", workflowId);

		apply(new BoardEvents.WorkflowCommitted(getBoardId(), workflowId, UUID.randomUUID(), DateProvider.now()));

		ensure(format("Workflow '%s' is committed", workflowId), () -> getCommittedWorkflow(workflowId).isPresent());
	}

	public void uncommitWorkflow(WorkflowId workflowId) {
		requireNotNull("Workflow id", workflowId);
		require(format("Workflow '%s' exists", workflowId), () -> getCommittedWorkflow(workflowId).isPresent());

		apply(new BoardEvents.WorkflowUncommitted(getBoardId(), workflowId, UUID.randomUUID(), DateProvider.now()));

		ensure(format("Workflow '%s' is uncommitted", workflowId), () -> !getCommittedWorkflow(workflowId).isPresent());
	}

	public void becomeBoardMember(BoardRole boardRole, String userId) {
		requireNotNull("User id", userId);

		if (existBoardMemberWithSameRole(userId, boardRole)){
			return;
		}

		apply(new BoardEvents.BoardMemberAdded(
				userId,
				getBoardId(),
				boardRole,
				UUID.randomUUID(),
				DateProvider.now()));


		ensure(format("User '%s' becomes a board member", userId), () -> getBoardMember(userId).isPresent());
		ensure(format("User role is '%s'", boardRole), () -> getBoardMember(userId).get().getBoardRole().equals(boardRole));
	}


	public boolean isBoardMember(String userId){
		requireNotNull("User id", userId);

		return getBoardMember(userId).isPresent();
	}

	public void removeBoardMember(String userId) {
		requireNotNull("User id", userId);

		if (isBoardMember(userId)){
			apply(new BoardEvents.BoardMemberRemoved(userId, getBoardId(), UUID.randomUUID(), DateProvider.now()));
		}

		ensure(format("Board member '%s' is removed", userId), () -> !getBoardMember(userId).isPresent());
	}

	public void moveWorkflow(WorkflowId workflowId, String userId, int order) {
		requireNotNull("Workflow id", workflowId);
		requireNotNull("User id", userId);
		require("Order >= 0",  () -> order >= 0);
		require("Order <= committed workflow size of board",
				() -> order <= getCommittedWorkflows().size());

		apply(new BoardEvents.WorkflowMoved(id, workflowId, userId, order, UUID.randomUUID(), DateProvider.now()));

		ensure("Committed workflow is in correct order",
				() -> getCommittedWorkflows().stream().anyMatch(x-> x.getWorkflowId().equals(workflowId) && x.getOrder() == order));
		ensure("All committed workflow is in correct order", () -> all_committed_workflow_is_in_correct_order());
	}

	private boolean all_committed_workflow_is_in_correct_order() {
		for(int i = 0; i< committedWorkflows.size(); i++){
			if(committedWorkflows.get(i).getOrder()!=i)
				return false;
		}
		return true;
	}

	public String getName() {
		return name;
	}

	public BoardId getBoardId() {
		return getId();
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		requireNotNull("Team id", teamId);
		this.teamId = teamId;
	}

	public Optional<BoardMember> getBoardMember(String userId){
		requireNotNull("User id", userId);

		for(BoardMember each : boardMembers){
			if(each.getUserId().equalsIgnoreCase(userId)){
				return Optional.of(each);
			}
		}
		return Optional.empty();
	}

	public List<BoardMember> getBoardMembers() {
		return boardMembers;
	}

	public void setBoardMembers(List<BoardMember> boardMembers) {
		requireNotNull("Board members", boardMembers);
		this.boardMembers = boardMembers;
	}

	public List<CommittedWorkflow> getCommittedWorkflows() {
		return committedWorkflows;
	}

	public void setCommittedWorkflows(List<CommittedWorkflow> committedWorkflows) {
		requireNotNull("Committed workflows", committedWorkflows);
		this.committedWorkflows = committedWorkflows;
	}

    public Optional<BoardMember> getBoardMemberById(String userId) {
		requireNotNull("User id", userId);

		return boardMembers.stream().
				filter( x -> x.getUserId().equals(userId)).
				findFirst();
    }

	public Optional<CommittedWorkflow> getCommittedWorkflow(WorkflowId workflowId) {
		requireNotNull("Workflow id", workflowId);

		return committedWorkflows
				.stream()
				.filter(x->x.getWorkflowId().equals(workflowId))
				.findFirst();
	}



	@Override
	public void apply(DomainEvent event){
		requireNotNull("event", event);

		when((BoardEvents) event);
		ensureValidState();
		addDomainEvent(event);
	}


	public void ensureValidState(){

	}

	private void configEventHandlers(){
		eventHandlers.put(BoardEvents.BoardCreated.class,
				this::handleBoardCreated);

		eventHandlers.put(BoardEvents.BoardDeleted.class,
				this::handleBoardDeleted);

		eventHandlers.put(BoardEvents.BoardMemberAdded.class,
				this::handleBoardMemberAdded);

		eventHandlers.put(BoardEvents.BoardMemberRemoved.class,
				this::handleBoardMemberRemoved);

		eventHandlers.put(BoardEvents.BoardRenamed.class,
				this::handleBoardRenamed);

		eventHandlers.put(BoardEvents.WorkflowCommitted.class,
				this::handleWorkflowCommitted);

		eventHandlers.put(BoardEvents.WorkflowUncommitted.class,
				this::handleWorkflowUncommitted);

		eventHandlers.put(BoardEvents.WorkflowMoved.class,
				this::handleWorkflowMoved);


	}


	private void when(BoardEvents event) {
		eventHandlers.getOrDefault(event.getClass(), null).accept(event);
	}

	private void handleBoardCreated(BoardEvents event) {
		BoardEvents.BoardCreated boardCreated = ((BoardEvents.BoardCreated) event);
		id = boardCreated.getBoardId();
		this.name = boardCreated.getBoardName();
		this.teamId = boardCreated.getTeamId();
		boardMembers = new ArrayList<>();
		committedWorkflows = new ArrayList<>();
	}

	private void handleBoardDeleted(BoardEvents boardEvents) {
		boardMembers.clear();
		committedWorkflows.clear();
		isDeleted = true;
	}

	private void handleBoardMemberAdded(BoardEvents event) {
		BoardEvents.BoardMemberAdded boardMemberAdded = ((BoardEvents.BoardMemberAdded) event);
		BoardMember boardMember = BoardMemberBuilder.newInstance()
				.memberType(boardMemberAdded.getBoardRole())
				.boardId(boardMemberAdded.getBoardId())
				.userId(boardMemberAdded.getUserId())
				.build();
		boardMembers.add(boardMember);
	}

	private void handleBoardMemberRemoved(BoardEvents event) {
		BoardEvents.BoardMemberRemoved boardMemberRemoved = ((BoardEvents.BoardMemberRemoved) event);
		getBoardMembers().removeIf(x -> x.getUserId().equals(boardMemberRemoved.getUserId()));
	}


	private void handleBoardRenamed(BoardEvents event) {
		BoardEvents.BoardRenamed boardRenamed = ((BoardEvents.BoardRenamed) event);
		name = boardRenamed.getBoardName();
	}

	private void handleWorkflowCommitted(BoardEvents event) {
		BoardEvents.WorkflowCommitted workflowCommitted = ((BoardEvents.WorkflowCommitted) event);
		addCommittedWorkflow(workflowCommitted.getWorkflowId());
	}

	private void handleWorkflowUncommitted(BoardEvents event) {
		BoardEvents.WorkflowUncommitted workflowUncommitted = ((BoardEvents.WorkflowUncommitted) event);
		removeCommittedWorkflow(workflowUncommitted.getWorkflowId());
	}

	private void handleWorkflowMoved(BoardEvents event) {
		BoardEvents.WorkflowMoved workflowMoved = (BoardEvents.WorkflowMoved) event;
		CommittedWorkflow committedWorkflow = getCommittedWorkflow(workflowMoved.getWorkflowId()).get();
		committedWorkflows.remove(committedWorkflow);
		committedWorkflows.add(workflowMoved.getOrder(), committedWorkflow);
		reorderWorkflow();
	}

	private void addCommittedWorkflow(WorkflowId workflowId) {
		int order = 0;
		if(committedWorkflows.size() > 0) {
			order = committedWorkflows.get(committedWorkflows.size()-1).getOrder() + 1;
		}
		CommittedWorkflow committedWorkflow = new CommittedWorkflow(getBoardId(), workflowId, order);
		committedWorkflows.add(committedWorkflow);
	}

	private void removeCommittedWorkflow(WorkflowId workflowId) {
		committedWorkflows.removeIf(x-> x.getWorkflowId().equals(workflowId));
		reorderWorkflow();
	}

	private boolean existBoardMemberWithSameRole(String userId, BoardRole boardRole) {
		return getBoardMember(userId).isPresent() &&
				getBoardMember(userId).get().getBoardRole().equals(boardRole);
	}

	private void reorderWorkflow() {
		for(int i = 0; i < committedWorkflows.size(); i++){
			committedWorkflows.get(i).setOrder(i);
		}
	}
}
