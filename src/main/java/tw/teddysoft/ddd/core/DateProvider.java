package tw.teddysoft.ddd.core;

import java.time.Instant;

public class DateProvider {
	private static Instant instant = null;
	
	public static Instant now() {
		if(instant == null) {
			return Instant.now();
		}
		return instant;
	}
	
	public static void setDate(Instant now) {
		instant = now;
	}

	public static void resetDate() {
		instant = null;
	}
}
