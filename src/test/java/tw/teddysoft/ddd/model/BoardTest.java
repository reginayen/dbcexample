package tw.teddysoft.ddd.model;

import tw.teddysoft.dbc.PreconditionViolationException;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import tw.teddysoft.ddd.model.*;

import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BoardTest {
    private String userId = "userId";

    private Board createBoard(){
        return new Board("teamId", BoardId.create(), "Scrum Board");
    }

    @Test
    public void create_a_valid_board() {
        BoardId boardId = BoardId.create();
        String teamId = "teamId";
        String boardName = "Scrum Board";

        Board board = new Board(teamId, boardId, boardName);

        assertEquals(teamId, board.getTeamId());
        assertEquals(boardId, board.getBoardId());
        assertEquals(boardName, board.getName());
        assertEquals(0, board.getCommittedWorkflows().size());
        assertEquals(0, board.getBoardMembers().size());
        assertFalse(board.isDeleted());
    }

    @Test
    public void commit_a_workflow() {
        Board board = createBoard();
        WorkflowId workflowId = WorkflowId.create();

        board.commitWorkflow(workflowId);

        assertEquals(1, board.getCommittedWorkflows().size());
        CommittedWorkflow committedWorkflow = board.getCommittedWorkflows().get(0);
        assertEquals(board.getBoardId(), committedWorkflow.getBoardId());
        assertEquals(workflowId, committedWorkflow.getWorkflowId());
    }

    @Test
    public void uncommit_an_existing_workflow() {
        Board board = createBoard();
        WorkflowId workflowId = WorkflowId.create();
        board.commitWorkflow(workflowId);

        board.uncommitWorkflow(workflowId);

        assertEquals(0, board.getCommittedWorkflows().size());
    }

    @Test
    public void uncommit_a_non_existing_workflow_throws_a_precondition_violation_exception() {
        Board board = createBoard();
        board.commitWorkflow(WorkflowId.create());

        WorkflowId workflowId = WorkflowId.create();
        Error exception = assertThrows(PreconditionViolationException.class, () -> {
            board.uncommitWorkflow(workflowId);
        });

        assertTrue(exception.getMessage().contains(format("Workflow '%s' exists", workflowId)));
        assertEquals(1, board.getCommittedWorkflows().size());
    }

    @Test
    public void move_the_first_workflow_to_the_last(){
        Board board = createBoard();
        WorkflowId workflowId = WorkflowId.create();
        board.commitWorkflow(workflowId);
        board.commitWorkflow(WorkflowId.create());
        board.commitWorkflow(WorkflowId.create());
        board.commitWorkflow(WorkflowId.create());

        board.moveWorkflow(workflowId, userId, 3);

        assertEquals(3, board.getCommittedWorkflow(workflowId).get().getOrder());
        for(int i = 0; i< board.getCommittedWorkflows().size(); i++) {
            assertEquals(i, board.getCommittedWorkflows().get(i).getOrder());
        }
    }

    @ParameterizedTest
    @EnumSource(BoardRole.class)
    public void add_a_board_member_increases_the_number_of_board_member_by_one(BoardRole role) {
        Board board = createBoard();

        board.becomeBoardMember(role, userId);

        assertEquals(1, board.getBoardMembers().size());
        BoardMember boardMember = board.getBoardMembers().get(0);
        assertEquals(board.getBoardId(), boardMember.getBoardId());
        assertEquals(role, boardMember.getBoardRole());
        assertEquals(userId, boardMember.getUserId());
    }

    @Test
    public void add_the_same_board_member_twice_does_nothing() {
        Board board = createBoard();
        board.becomeBoardMember(BoardRole.Member, userId);
        assertEquals(1, board.getBoardMembers().size());

        board.becomeBoardMember(BoardRole.Member, userId);

        assertEquals(1, board.getBoardMembers().size());
    }


    @Test
    public void remove_an_valid_board_member_decreases_the_number_of_board_member_by_one() {
        Board board = createBoard();
        board.becomeBoardMember(BoardRole.Admin, userId);
        assertEquals(1, board.getBoardMembers().size());

        board.removeBoardMember(userId);

        assertEquals(0, board.getBoardMembers().size());

    }

    @Test
    public void remove_an_invalid_board_member_does_nothing() {
        Board board = createBoard();
        board.becomeBoardMember(BoardRole.Admin, userId);
        assertEquals(1, board.getBoardMembers().size());

        board.removeBoardMember(UUID.randomUUID().toString());

        assertEquals(1, board.getBoardMembers().size());
    }


    @Test
    public void change_a_board_name_to_a_new_name() {
        Board board = createBoard();

        board.rename("newBoardName");

        assertEquals("newBoardName", board.getName());
    }

}
